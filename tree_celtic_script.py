#!/usr/bin/env python
# coding: utf-8

# This python script uses tree sources : 
# - the database of the "inventaire forestier", all information of the raw data can be found here : https://inventaire-forestier.ign.fr/spip.php?rubrique90
# - the list of bruxelles mobilies trees : all information here : https://data.mobility.brussels/fr/info/trees/
# - the list of vertues linked to tree : 
# 
# The main output are 2 set of map of json files : 
#The first set covers french forest, the second set Bruxelles main road ; there is 2 maps and 2 json files for each essence, if the essence is present for both region
# 
# 



#Library importation
import numpy as np
import pandas as pd
from pyproj import Proj, transform # transforme les données lambert en latitude longitude
import folium 
inProj = Proj(init='epsg:2154')#rérence des lambert 93
outProj = Proj(init='epsg:4326')#références longitude latitude

import matplotlib.pyplot as plt
import geopandas as gpd

path="C:\\Users\\albuisson\\Desktop\\Projets AL Perso\\arbres_vertues\\"






#importation forest France + merge of referentiel with data
#tree data + map France
trees0=pd.read_csv(path+"data/arbres_foret_2018.csv",sep = ';',encoding='cp1252')
placettes0=pd.read_csv(path+"data/placettes_foret_2018.csv",sep = ';',encoding='cp1252')
ref=pd.read_csv(path+"data/documentation_2018_enrichi.csv",encoding='cp1252')
fp = path+"data/ser_l93/ser_l93_new.shp"
map_df0 = gpd.read_file(fp)# check data type 
map_df=map_df0.to_crs({'init': 'epsg:4326'})
map_df_pd=pd.DataFrame(map_df)

ref_species=ref[ref['donnee']=='ESPAR']
ref_places=ref[ref['donnee']!='ESPAR'].rename(columns={'libelle':'zone'})
placettes1=placettes0[['idp','ser']]
trees1=trees0.merge(placettes1, how='left', on=None, left_on=['idp'], right_on=['idp'])
trees2 = trees1.merge(ref_species[['code','essence']], how='left', on=None, left_on=['espar'], right_on=['code'])
db0 = trees2.merge(ref_places[['code','zone']], how='left', on=None, left_on=['ser'], right_on=['code'])
db_france=db0.fillna('other')
db_france.head(1)






#import brussel's trees
f = open(path+"data/tree_bxl_all.txt")
rows = []
for line in f:
    row = line.split("\t")     # Split on tab characters
    rows.append(row)    # Append to our list of lists:

tree_bxl=pd.DataFrame(rows[1:], columns=rows[0])
tree_bxl["geometry"]=tree_bxl["geom\n"].apply(lambda x:x[7:-2])

tree_bxl["geometry"] = tree_bxl["geometry"].apply(lambda x :x.replace(" ", ","))
tree_bxl["geometry"] = tree_bxl["geometry"].apply(lambda x :x.split(","))


tree_bxl["essence"]=tree_bxl["english_species"].apply( lambda x: x.lower())

from shapely.geometry import Point
tree_bxl['geom']=tree_bxl['geometry'].apply(lambda x :Point(float(x[0]), float(x[1])))


tree_bxl.head(1)





#importation data celtic_value
f = open(path+"data/list_vertues2.txt")
rows = []
for line in f:
    row = line.split("\t")     # Split on tab characters
    rows.append(row)    # Append to our list of lists:

virtues=pd.DataFrame(rows[1:], columns=rows[0])
virtues['Species_English']=virtues['Species_English'].apply( lambda x: x.lower())
virtues.head(1)






###########################
#### Functions Virtues ####
###########################
def from_tree_to_virtue(tree,virtues=virtues):
    virtue=virtues['celtic_value'][virtues['Species_English']==tree].tolist()
    return virtue

#function vertues / Arbres
def from_virtue_to_tree(virtue,virtues=virtues):
    species=virtues['Species_English'][virtues['celtic_value']==virtue].tolist()

    return species






#################################
#### Functions French forest ####
#################################


def presence_essence_france(essence, db_france=db_france): 
    db_france['essence']=db_france['essence'].apply( lambda x: x.lower())
    a=db_france[db_france['essence']==essence].shape
    result=1
    list_essence=pd.unique(db_france['essence'])
    if a[0]==0:
        print("Sorry, this species doesn't exist. Please, chose one of the following : ")
        print(list_essence)
        result=0
    return result



# proportion of placette having a given specie in a given zone
def prop_plaq_essence(zone,essence,db_france=db_france):
    essence=essence.lower()
    tab_zone=db_france[db_france['ser']==zone]
    tab_zone_essence=tab_zone[tab_zone['essence']==essence]
    nb_placettes=tab_zone.idp.nunique()
    prop_placette_essence=0
    #print(nb_placettes)
    if nb_placettes>0: 
        nb_placettes_essence=tab_zone_essence.idp.nunique()
        list_prop_placette_essence=nb_placettes_essence/nb_placettes
    return list_prop_placette_essence

#identifie the zone with the biggest proportion of placettes with one given species. 
#Use the prop_plaq_essence functino
def top_prop_essence (essence,db_france=db_france):
    zones=np.unique(db_france['ser']) 
    prop=0
    for zone in zones:
        prop1=prop_plaq_essence(zone,essence,db_france)
        if prop1>prop:
            prop=prop1
            zone_top=zone
    print('the proportion of inventarie point in the zone with the specie is ' + str(prop))
   # print(zone_top)
    zone_texte=str(ref_places[ref_places['code']==zone_top]["zone"])
    print("The best place to see "+ essence +" in France is probably " + zone_texte)
    return zone_top


#For a given zone and a given specie, wich placette of the zone does have 
#the biggest proportion of trees from this species ? 
def point_zone(zone,essence,db_france=db_france):
    tab_zone=db_france[db_france['ser']==zone]
    essence=essence.lower()
    tab_zone_essence=tab_zone[tab_zone['essence']==essence]
    points=pd.unique(tab_zone['idp'])
    prop=0
    for point in points:
        tab_zone_point=tab_zone[tab_zone['idp']==point]
        tab_zone_essence_point=tab_zone_essence[tab_zone_essence['idp']==point]
        if (tab_zone_point.a.nunique())>0:
            prop1=tab_zone_essence_point.a.nunique()/tab_zone_point.a.nunique()
            if prop1>prop:
                prop=prop1
                idp_top=point
    #print(point_top)
    return idp_top
    

#For a given 'placette', what are the gps coordonnate of the central point ? + produce the link to open street map
def pin_point_xy(point_cibl):
    x1=int(placettes0[placettes0['idp']==point_cibl]['xl93'])
    y1=int(placettes0[placettes0['idp']==point_cibl]['yl93'])
    x,y = transform(inProj,outProj,x1,y1) #fonction de transformation des coordonnées
    return x,y


##########preparation of the map
# Average of the proportion of a specie in the plaquelle of a zone
def prop_essence(zone,essence,db_france=db_france ):
    essence=essence.lower()
    tab_zone=db_france[db_france['ser']==zone]
    tab_zone_essence=tab_zone[tab_zone['essence']==essence]
    prop_placette_essence=[]
    prop_placette_essence = pd.DataFrame(columns=['idp','prop'])
    prop_placette_essence['idp']=pd.unique(tab_zone['idp'])
    prop_placette_essence['prop']=0
    avgprop_placette_essence=0
    idps=pd.unique(tab_zone['idp'])
    for idp in idps:
        if tab_zone.shape[0]>0:
            prop_placette_essence['prop']=(tab_zone_essence[tab_zone_essence['idp']==idp].shape[0]/
                                           tab_zone[tab_zone['idp']==idp].shape[0])
    avgprop_placette_essence=np.mean(prop_placette_essence['prop'])
    return avgprop_placette_essence


def prop_essence_zone (essence,db_france=db_france):
    essence=essence.lower()
    zones=pd.unique(db_france['ser']) 
    prop=0
    tab_prop_zone={}
    for zone in zones:  
        tab_prop_zone[zone]=prop_essence(zone,essence)
#    print(tab_prop_zone)
    return tab_prop_zone



##plot OSM  - Region France Forest
def create_folium_map_france_forest(essence): 
        
    #What is the best zone, in this zone, the best placette, and where is it's central point ? 
    zone=top_prop_essence(essence)
    point_cibl=point_zone(zone,essence)
    x,y = pin_point_xy(point_cibl)
    print(x, y)

    ##for the map of France's regions
    proportion0=prop_essence_zone(essence)
    proportion=pd.DataFrame(list(proportion0.items()),columns = ['zone','prop']) 
    df_plot_proportion1=map_df.merge(proportion,how='left', on=None, left_on=['codeser'], right_on=['zone'])
    df_plot_proportion0=df_plot_proportion1[df_plot_proportion1['prop']>0.001].reset_index()

#    print(df_plot_proportion0.shape)
    m = folium.Map(
      location=[47, 2.7],
      zoom_start=6,
    tiles = "Stamen Toner", ##Stamen Toner  
    )    
    ##add best point to the graph
    folium.CircleMarker([float(y),float(x)],radius=6,color='red',fill_color ='red',popup='Best Place',tooltip=essence).add_to(m)
    ##add zones to the graph, colored by proportion
    for i in range(0 , df_plot_proportion0.shape[0]-1):
        a=round((df_plot_proportion0["prop"][i]),2)
        style_function=lambda feature, a=a: {
                        'fillColor': 'green', #border color for the color fills
                        'fillOpacity' : a,
                        'color': 'black',
                         'weight': 0.6         #how thick the border has to be
                        }
        folium.GeoJson(data=df_plot_proportion0["geometry"][i],style_function=style_function).add_to(m)        
    m.save(path+"/French_Forest_"+essence+".html")
    print("You can find the map here : "+path+"/French_Forest_"+essence+".html")

def create_geojson_france_forest(essence): 
        
    #What is the best zone, in this zone, the best placette, and where is it's central point ? 
    zone=top_prop_essence(essence)
    point_cibl=point_zone(zone,essence)
    x,y = pin_point_xy(point_cibl)
 
    ##for the map of France's regions
    proportion0=prop_essence_zone(essence)
    proportion=pd.DataFrame(list(proportion0.items()),columns = ['zone','prop']) 
    df_plot_proportion1=map_df.merge(proportion,how='left', on=None, left_on=['codeser'], right_on=['zone'])
    df_plot_proportion0=df_plot_proportion1[df_plot_proportion1['prop']>0.000].reset_index()

   if df_plot_proportion0.shape[0]>0:
        print(type(df_plot_proportion0))
        df_plot_proportion0.to_json()
        df_plot_proportion0.to_file(path+"/geojson_french_forest_"+essence+".json", driver="GeoJSON")  






#######################################
#####  Function Trees Brussels ########
#######################################


tree_bxl['Status']=tree_bxl['status']
tree_bxl['Status']=tree_bxl['Status'].replace({  'en vie' :'Alive',
                         'enlevÇ¸' :'Removed',
                         'mort':'Dead'  ,
                         'souche': 'Stump',                       
                         'chandelle': 'Stump'
 })
colordict = {'Alive': 'limegreen', 'Removed': 'grey', 'Dead': 'red', 'Stump': 'orange'}
#tree_bxl['Status'].unique()

def presence_essence_bruxelles(essence): 
 tree_bxl['essence']=tree_bxl['essence'].apply( lambda x: x.lower())
 a=tree_bxl[tree_bxl['essence']==essence].shape
 result=1
 list_essence=pd.unique(tree_bxl['essence'])
 if a[0]==0:
#        print("Sorry, this species doesn't exist. Please, chose one of the following : ")
#        print(list_essence)
     result=0
 return result    

def presence_essence_brussels(essence,tree_bxl=tree_bxl ): 
 tree_bxl['essence']=tree_bxl['english_species'].apply( lambda x: x.lower())
 a=tree_bxl[tree_bxl['essence']==essence].shape
 result=1
 list_essence=pd.unique(tree_bxl['essence'])
 if a[0]==0:
     print("Sorry, this species doesn't exist. Please, chose one of the following : ")
     print(list_essence)
     result=0
 return result




##plot OSM  - Brussels's trees
def create_folium_map_brussels_trees(essence,tree_bxl=tree_bxl): 
 tree_bxl_liste=tree_bxl[tree_bxl['essence']==essence].reset_index()
 print(tree_bxl['Status'].unique())
 m = folium.Map(
   location=[50.85, 4.4],
   zoom_start=10,
 tiles = "Stamen Toner", ##Stamen Toner  
 )
 print(tree_bxl_liste.shape[0])
 for i in range(0 , tree_bxl_liste.shape[0]-1):
     statut=tree_bxl['Status'][i]
     #print(colordict[color])
     folium.CircleMarker([tree_bxl_liste['geometry'][i][1],tree_bxl_liste['geometry'][i][0]] ,
                         radius=2.5,color=colordict[statut],fill_color =colordict[statut],
                         fill=True,fill_opacity=1,
                         tooltip=essence +' - '+statut).add_to(m)
 #    print(tree_bxl_liste['geometry'][i])
 m.save(path+"/trees_BXL_"+essence+".html")

def create_geojson_brussels_trees(essence): 
     
 #What is the best zone, in this zone, the best placette, and where is it's central point ? 
 tree_bxl_liste0=pd.DataFrame(tree_bxl[tree_bxl['essence']==essence].reset_index())
 tree_bxl_liste1=tree_bxl_liste0[['Status', 'essence', 'geom']]
# print((tree_bxl_liste0))
 tree_bxl_liste = gpd.GeoDataFrame(tree_bxl_liste1, geometry='geom')
 print(type(tree_bxl_liste))

 tree_bxl_liste.to_file(path+"/geojson_trees_BXL_"+essence+".json", driver="GeoJSON")  






#Example of use : 

#list of essence
essence0=from_virtue_to_tree('Centering')
essence= map(str.lower,essence0)

## generate map for all essence in the vertue list
list_essence0=list(virtues['Species_English'].unique())
list_essence= map(str.lower,list_essence0)

for essence in list_essence: 
    essence_exist_FR=presence_essence_france(essence)
    essence_exist_BXL=presence_essence_bruxelles(essence)
    if essence_exist_FR==1:
        create_folium_map_france_forest(essence) 
        create_geojson_france_forest(essence)  
    if essence_exist_BXL==1:
        create_geojson_brussels_trees(essence)
        create_folium_map_brussels_trees(essence)





